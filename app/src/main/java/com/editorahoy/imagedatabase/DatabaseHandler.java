package com.editorahoy.imagedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DatabaseHandler  extends SQLiteOpenHelper {

    public DatabaseHandler(@Nullable Context context) {
        super(context, "imageDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table images(id integer primary key, img blob not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("drop table if exists images");
    }

    public  Boolean insertimage(String x, Integer i){
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            FileInputStream fs = new FileInputStream(x);
            byte[] imgbyte = new byte[fs.available()];
            fs.read(imgbyte);

            ContentValues contentValues = new ContentValues();
            contentValues.put("id",i);
            contentValues.put("img",imgbyte);
            db.insert("images",null,contentValues);

            fs.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
