package com.editorahoy.imagedatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button insert;
    EditText number;
    DatabaseHandler db;
    private static final int PINCK_IMAGE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        insert = (Button) findViewById(R.id.insert);
        number = (EditText) findViewById(R.id.number);
        db = new DatabaseHandler(this);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, Uri.parse(
                       // "content://storage/emulated/0/Pictures"
                        "content://media/internal/images/media"
                ));
                startActivityForResult(intent, PINCK_IMAGE);
            }
        });
    }

    protected  void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK && requestCode==PINCK_IMAGE){
            Uri uri = data.getData();
            String x = getPath(uri);
            Integer num = Integer.parseInt(number.getText().toString());

            if (db.insertimage(x,num)){
                Toast.makeText(getApplicationContext(),"Successfull",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(),"Not Successfull",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String getPath(Uri uri){
        if (uri==null) return null;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri,projection,null,null,null);
        if (cursor!=null){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToNext();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }


}